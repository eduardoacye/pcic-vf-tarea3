(* Implementación de númers con paridad y sus funciones y relaciones asociadas *)

Inductive bn : Type :=
| Z
| U (b : bn)
| D (b : bn).

(* Sucesor *)
Fixpoint bn_suc (b : bn) : bn :=
  match b with
  | Z => U Z
  | U b' => D b'
  | D b' => U (bn_suc b')
  end.

(* Predecesor *)
Fixpoint bn_pre (b : bn) : bn :=
  match b with
  | Z => Z
  | U Z => Z
  | U b' => D (bn_pre b')
  | D b' => U b'
  end.

(* Transformaciones desde y hacia naturales unitarios nat *)
Fixpoint bn_nat (b : bn) : nat :=
  match b with
  | Z => O
  | U b' => S (2 * bn_nat b')
  | D b' => S (S (2 * bn_nat b'))
  end.

Fixpoint nat_bn (n : nat) : bn :=
  match n with
  | O => Z
  | S n' => bn_suc (nat_bn n')
  end.

(* Suma *)
Fixpoint bn_plus (b1 b2 : bn) : bn :=
  match b1, b2 with
  | Z, b2 => b2
  | b1, Z => b1
  | U b1', U b2' => D (bn_plus b1' b2')
  | D b1', U b2' => U (bn_suc (bn_plus b1' b2'))
  | U b1', D b2' => U (bn_suc (bn_plus b1' b2'))
  | D b1', D b2' => D (bn_suc (bn_plus b1' b2'))
  end.

Notation "a bn+ b" := (bn_plus a b) (at level 60).

(* Relación de orden *)
Inductive bn_lt : bn -> bn -> Prop :=
| bn_lt_ZU (b : bn)                       : bn_lt Z (U b)
| bn_lt_ZD (b : bn)                       : bn_lt Z (D b)
| bn_lt_UU (b1 b2 : bn) (H : bn_lt b1 b2) : bn_lt (U b1) (U b2)
| bn_lt_UDeq (b : bn)                     : bn_lt (U b) (D b)
| bn_lt_UD (b1 b2 : bn) (H : bn_lt b1 b2) : bn_lt (U b1) (D b2)
| bn_lt_DU (b1 b2 : bn) (H : bn_lt b1 b2) : bn_lt (D b1) (U b2)
| bn_lt_DD (b1 b2 : bn) (H : bn_lt b1 b2) : bn_lt (D b1) (D b2).

Inductive bn_le : bn -> bn -> Prop :=
| bn_le_id (b : bn) : bn_le b b
| bn_le_lt (b1 b2 : bn) (H : bn_lt b1 b2) : bn_le b1 b2.

Notation "a bn< b" := (bn_lt a b) (at level 70).
Notation "a bn< b bn< c" := (bn_lt a b /\ bn_lt b c) (at level 70, b at next level).
Notation "a bn<= b" := (bn_le a b) (at level 70).

