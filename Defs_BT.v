(* Implementación de árboles de Braun y funciones y relaciones asociadas *)
From Tarea Require Export Defs_BN.

Parameter (A:Type)
          (eq_dec_A: forall (x y:A),{x=y}+{x<>y})
          (undefA : A).

Inductive BTree : Type :=
| E : BTree   
| N : A -> BTree  -> BTree  -> BTree.

Parameter (undefBTree : BTree).

Fixpoint bsize (t : BTree) : bn :=
  match t with
  | E => Z
  | N x s t =>  bn_suc ((bsize s) bn+ (bsize t))
  end.

Inductive bbal : BTree -> Prop:= 
| bbalE : bbal E 
| bbalN : forall (a: A) (s t: BTree),
    bbal s -> bbal t ->
    (bsize t) bn<= (bsize s) ->
    (bsize s) bn<= (bn_suc (bsize t)) ->
    bbal (N a s t).

Parameter (allBal: forall (t:BTree), bbal t).

Fixpoint lookup_bn (t : BTree) (b : bn) : A :=
  match t,b with
  | E, b => undefA
  | N x s t, Z => x 
  | N x s t, U a => lookup_bn s a   (* U a = 2a+1 *)
  | N x s t, D a => lookup_bn t a   (* D a = 2a + 2 *) 
  end.

Fixpoint update (t : BTree) (b : bn) (x : A) : BTree :=
  match t,b with
  | E, b => undefBTree
  | N y s t, Z =>  N x s t
  | N y s t, U a => N y (update s a x) t
  | N y s t, D a => N y s (update t a x)
  end.

Fixpoint le (x : A) (t : BTree) : BTree :=
  match t with
  | E => N x E E
  | N y s t => N x (le y t) s
  end.

Fixpoint he (x : A) (t : BTree) : BTree  :=
  match t with
  | E => N x E E
  | N y l r => match bsize t with
               | U b => N y (he x l) r
               | D b => N y l (he x r)
               | Z => undefBTree 
               end
  end.

Fixpoint lr (t : BTree) : BTree :=
  match t with
  | E => undefBTree
  | N x l r => match l with
               | E => E
               | N y _ _ => N y r (lr l)
               end
  end.

Fixpoint hr (t : BTree) : BTree :=
  match bsize t with
  | Z => undefBTree
  | U Z => E
  | U _ =>                      (* El árbol está balanceado *)
    match t with
    | E => undefBTree           (* Nunca pasa *)
    | N x l r => N x l (hr r)
    end
  | D _ =>                      (* El árbol izquierdo es mas grande que el derecho *)
    match t with
    | E => undefBTree           (* Nunca pasa *)
    | N x l r => N x (hr l) r
    end
  end.

