(* Resultados de números con paridad vistos en clase *)
From Tarea Require Export Defs_BN.
Require Import Setoid.
Require Import Lia.

(* UInj *)
Lemma bn_inj_U : forall (b1 b2 : bn), U b1 = U b2 -> b1 = b2.
Proof.
  intros.
  inversion H.
  reflexivity.
Qed.

(* DInj *)
Lemma bn_inj_D : forall (b1 b2 : bn), D b1 = D b2 -> b1 = b2.
Proof.
  intros.
  inversion H.
  reflexivity.
Qed.

(* ZnotU *)
Lemma bn_neq_ZU : forall (b : bn), Z <> U b.
Proof. intros. discriminate. Qed.

(* ZnotD *)
Lemma bn_neq_ZD : forall (b : bn), Z <> D b.
Proof. intros. discriminate. Qed.

(* UnotD y DnotU *)
Lemma bn_neq_UD : forall (b1 b2 : bn), U b1 <> D b2.
Proof. intros. discriminate. Qed.

Lemma bn_neq_sym : forall (b1 b2 : bn), b1 <> b2 -> b2 <> b1.
Proof. intros. intro. rewrite H0 in H. apply H. reflexivity. Qed.

(* bnotU *)
Lemma bn_neq_U : forall (b : bn), b <> U b.
Proof.
  intros. induction b.
  - intro. discriminate H.
  - intro. inversion H. apply IHb. apply H1.
  - intro. inversion H.
Qed.

(* bnotD *)
Lemma bn_neq_D : forall (b : bn), b <> D b.
Proof.
  intros. induction b.
  - intro. discriminate H.
  - intro. inversion H.
  - intro. inversion H. apply IHb. apply H1.
Qed.

(* U_not *)
Lemma bn_neq_UU : forall (b1 b2 : bn), U b1 <> U b2 -> b1 <> b2.
Proof. intros. contradict H. apply f_equal. apply H. Qed.

(* D_not *)
Lemma bn_neq_DD : forall (b1 b2 : bn), D b1 <> D b2 -> b1 <> b2.
Proof. intros. contradict H. apply f_equal. apply H. Qed.

(* dec_eq_BN *)
Theorem bn_dec_eq : forall (b1 b2 : bn), {b1 = b2} + {b1 <> b2}.
Proof.
  intro. induction b1.
  - intro. destruct b2.
    + left. reflexivity.
    + right. apply bn_neq_ZU.
    + right. apply bn_neq_ZD.
  - intro. destruct b2.
    + right. apply bn_neq_sym. apply bn_neq_ZU.
    + destruct (IHb1 b2) as [H | H].
      * left. rewrite H. reflexivity.
      * right. intro. apply H. inversion H0. reflexivity.
    + right. apply bn_neq_UD.
  - intro. destruct b2.
    + right. apply bn_neq_sym. apply bn_neq_ZD.
    + right. apply bn_neq_sym. apply bn_neq_UD.
    + destruct (IHb1 b2) as [H | H].
      * left. rewrite H. reflexivity.
      * right. intro. apply H. inversion H0. reflexivity.
Qed.

(* ZnotSucBN y suc_not_zero *)
Lemma bn_nsuc_Z : forall (b : bn), Z <> bn_suc b.
Proof.
  intro. destruct b.
  - simpl. apply bn_neq_ZU.
  - simpl. apply bn_neq_ZD.
  - simpl. apply bn_neq_ZU.
Qed.

(* notSucBN *)
Lemma bn_nsuc_id : forall (b : bn), b <> bn_suc b.
Proof.
  intro. destruct b.
  - simpl. apply bn_neq_ZU.
  - simpl. apply bn_neq_UD.
  - simpl. apply bn_neq_sym. apply bn_neq_UD.
Qed.

(* bnNonZ y UDCase *)
Lemma bn_nonZ : forall (b : bn), b <> Z -> exists (b' : bn), b = U b' \/ b = D b'.
Proof.
  intros.
  destruct b.
  - contradict H. reflexivity.
  - exists b. left. reflexivity.
  - exists b. right. reflexivity.
Qed.

(* predBNUD *)
Lemma bn_pre_UD : forall (b : bn), b <> Z -> bn_pre (U b) = D (bn_pre b).
Proof.
  intros. destruct b.
  - simpl. contradict H. reflexivity.
  - reflexivity.
  - reflexivity.
Qed.

(* predsucBNinv *)
Lemma bn_pre_suc_id : forall (b : bn), bn_pre (bn_suc b) = b.
Proof.
  intro. induction b.
  - simpl. reflexivity.
  - simpl. reflexivity.
  - simpl. destruct (bn_suc b) eqn:H.
    + symmetry in H. apply bn_nsuc_Z in H. destruct H.
    + apply f_equal. apply IHb.
    + apply f_equal. simpl in IHb. rewrite <- IHb. simpl. reflexivity.
Qed.

(* sucpredBNinv *)
Lemma bn_suc_pre_id : forall (b : bn), b <> Z -> bn_suc (bn_pre b) = b.
Proof.
  intros. induction b.
  - contradict H. reflexivity.
  - destruct b.
    + simpl. reflexivity.
    + assert (U b <> Z).
      { apply bn_neq_sym. apply bn_neq_ZU. }
      assert (IHb := (IHb H0)).
      rewrite <- IHb at 2. reflexivity.
    + assert (D b <> Z).
      { apply bn_neq_sym. apply bn_neq_ZD. }
      assert (IHb := (IHb H0)).
      rewrite <- IHb at 2. reflexivity.
  - simpl. reflexivity.
Qed.

(* toN_sucBN *)
Lemma bn_suc_nat_S : forall (b : bn), bn_nat (bn_suc b) = S (bn_nat b).
Proof.
  intros. induction b.
  - simpl. reflexivity.
  - simpl. reflexivity.
  - simpl. rewrite IHb. lia.
Qed.

(* sucBN_toBN *)
Lemma nat_S_bn_suc : forall (n : nat), bn_suc (nat_bn n) = nat_bn (S n).
Proof.
  intros. destruct n.
  - simpl. reflexivity.
  - simpl. reflexivity.
Qed.

(* inverse_op *)
Lemma bn_nat_id : forall (n : nat), bn_nat (nat_bn n) = n.
Proof.
  intros. induction n.
  - simpl. reflexivity.
  - simpl. rewrite bn_suc_nat_S. apply f_equal. apply IHn.
Qed.

Lemma nat_bn_U : forall (n : nat), nat_bn (S (2 * n)) = U (nat_bn n).
Proof.
  intros. induction n.
  - simpl. reflexivity.
  - assert (2 * S n = S (2 * n + 1)). lia.
    rewrite H. clear H.
    unfold nat_bn at 1. fold nat_bn.
    assert (2 * n + 1 = S (2 * n)). lia.
    rewrite H. clear H.
    rewrite IHn. simpl. reflexivity.
Qed.

Lemma nat_bn_D : forall (n : nat), nat_bn (S (S (2 * n))) = D (nat_bn n).
Proof.
  intros. induction n.
  - simpl. reflexivity.
  - unfold nat_bn at 1. fold nat_bn.
    assert (2 * S n = S (S (2 * n))). lia.
    rewrite H. clear H.
    rewrite IHn. simpl. reflexivity.
Qed.

(* inverse_op_2 *)
Lemma nat_bn_id : forall (b : bn), nat_bn (bn_nat b) = b.
Proof.
  intros.
  induction b.
  - simpl. reflexivity.
  - unfold bn_nat. fold bn_nat.
    rewrite nat_bn_U. rewrite IHb. reflexivity.
  - unfold bn_nat. fold bn_nat.
    rewrite nat_bn_D. rewrite IHb. reflexivity.
Qed.

(* SucBNinj *)
Lemma bn_suc_eq : forall (b1 b2 : bn), bn_suc b1 = bn_suc b2 -> b1 = b2.
Proof.
  intro. induction b1.
  - destruct b2.
    + simpl. reflexivity.
    + simpl. intros. discriminate H.
    + simpl. intros. inversion H. contradict H1. apply bn_nsuc_Z.
  - destruct b2.
    + simpl. intros. discriminate H.
    + simpl. intros. inversion H. reflexivity.
    + simpl. intros. discriminate H.
  - destruct b2.
    + simpl. intros. inversion H. symmetry in H1. contradict H1. apply bn_nsuc_Z.
    + simpl. intros. discriminate H.
    + simpl. intros. inversion H. rewrite (IHb1 b2 H1). reflexivity.
Qed.

(* plusBN_toN *)
Lemma bn_plus_nat : forall (b1 b2 : bn), bn_nat (b1 bn+ b2) = bn_nat b1 + bn_nat b2.
Proof.
  intros b1. induction b1.
  - simpl. reflexivity.
  - intros b2. destruct b2.
    + simpl. lia.
    + simpl. rewrite IHb1. lia.
    + simpl. rewrite bn_suc_nat_S. rewrite IHb1. lia.
  - intros b2. destruct b2.
    + simpl. lia.
    + simpl. rewrite bn_suc_nat_S. rewrite IHb1. lia.
    + simpl. rewrite bn_suc_nat_S. rewrite IHb1. lia.
Qed.

(* plus_neutro *)
Lemma bn_plus_neut : forall (b : bn), b bn+ Z = b.
Proof. intros. destruct b; (simpl; trivial). Qed.

(* plus_U *)
Lemma bn_plus_U : forall (b : bn), bn_suc (b bn+ b) = U b.
Proof.
  intros. induction b.
  - simpl. reflexivity.
  - simpl. rewrite IHb. reflexivity.
  - simpl. rewrite IHb. simpl. reflexivity.
Qed.

(* plus_D *)
Lemma bn_plus_D : forall (b : bn), bn_suc (bn_suc b bn+ b) = D b.
Proof.
  intros. induction b.
  - simpl. reflexivity.
  - simpl. rewrite bn_plus_U. reflexivity.
  - simpl. rewrite IHb. reflexivity.
Qed.

(* plusSuc *)
Lemma bn_plus_suc : forall (b1 b2 : bn), bn_suc (b1 bn+ b2) = bn_suc b1 bn+ b2.
Proof.
  intros b1. induction b1.
  - intros b2. destruct b2; (simpl; reflexivity).
  - intros b2. destruct b2; (simpl; reflexivity).
  - intros b2. destruct b2.
    + simpl. reflexivity.
    + simpl. rewrite IHb1. reflexivity.
    + simpl. rewrite IHb1. reflexivity.
Qed.

(* plus_toBN *)
Lemma bn_plus_nat_bn : forall (n1 n2 : nat), nat_bn (n1 + n2) = nat_bn n1 bn+ nat_bn n2.
Proof.
  intros. induction n1.
  - simpl. reflexivity.
  - simpl. rewrite IHn1. rewrite <- bn_plus_suc. reflexivity.
Qed.

(* plusComm *)
Lemma bn_plus_comm : forall (b1 b2 : bn), (b1 bn+ b2) = (b2 bn+ b1).
Proof.
  intros b1. induction b1.
  - intros b2. destruct b2.
    + simpl. reflexivity.
    + simpl. reflexivity.
    + simpl. reflexivity.
  - intros b2. destruct b2.
    + simpl. reflexivity.
    + simpl. rewrite (IHb1 b2). reflexivity.
    + simpl. rewrite (IHb1 b2). reflexivity.
  - intros b2. destruct b2.
    + simpl. reflexivity.
    + simpl. rewrite (IHb1 b2). reflexivity.
    + simpl. rewrite (IHb1 b2). reflexivity.
Qed.

(* plusSuc_2 *)
Lemma bn_plus_suc2 : forall (b1 b2 : bn), bn_suc (b1 bn+ b2) = b1 bn+ bn_suc b2.
Proof.
  intros b1 b2.
  assert (H := bn_plus_suc b2 b1).
  rewrite bn_plus_comm in H.
  rewrite H.
  rewrite bn_plus_comm.
  reflexivity.
Qed.

(* plusBN_Z_Z *)
Lemma bn_plus_ZZ : forall (b1 b2 : bn), b1 bn+ b2 = Z -> b1 = Z /\ b2 = Z.
Proof.
  intros. destruct b1, b2; try(simpl in H; discriminate).
  split; reflexivity.
Qed.

(* addition_a_a *)
Lemma bn_plus_eq : forall (b1 b2 : bn), b1 bn+ b1 = b2 bn+ b2 -> b1 = b2.
Proof.
  intros.
  apply (f_equal bn_suc) in H.
  rewrite bn_plus_U in H. rewrite bn_plus_U in H.
  apply bn_inj_U.
  apply H.
Qed.

(* addition_SucBNa_a *)
Lemma bn_plus_suc_eq : forall (b1 b2 : bn), bn_suc b1 bn+ b1 = bn_suc b2 bn+ b2 -> b1 = b2.
Proof.
  intros.
  rewrite <- bn_plus_suc in H.
  rewrite <- bn_plus_suc in H.
  apply bn_suc_eq in H.
  apply (f_equal bn_suc) in H.
  rewrite bn_plus_U in H.
  rewrite bn_plus_U in H.
  apply bn_inj_U.
  apply H.
Qed.

(* ltBN_arefl *)
Lemma bn_lt_arefl : forall (b : bn), ~ b bn< b.
Proof.
  intros. induction b.
  - intro. inversion H.
  - contradict IHb. inversion IHb. trivial.
  - contradict IHb. inversion IHb. trivial.
Qed.

Create HintDb BNDB.

Hint Resolve bn_lt_arefl : BNDB.

(* ltBN_asym *)
Lemma bn_lt_asym : forall (b1 b2 : bn), b1 bn< b2 -> ~ b2 bn< b1.
Proof.
  intros. induction H.
  - intro. inversion H.
  - intro. inversion H.
  - contradict IHbn_lt. inversion IHbn_lt. trivial.
  - intro. inversion H. apply (bn_lt_arefl b1). subst. trivial.
  - intro. inversion H0. subst. apply IHbn_lt. trivial.
  - contradict IHbn_lt. inversion IHbn_lt. rewrite H2 in H. trivial. subst. trivial.
  - contradict IHbn_lt. inversion IHbn_lt. trivial.
Qed.

Hint Resolve bn_lt_asym : BNDB.

(* ltBN_tr *)
Lemma bn_lt_tr : forall (b2 b3 : bn), b2 bn< b3 -> forall (b1 : bn), b1 bn< b2 -> b1 bn< b3.
Proof. Admitted.

Hint Resolve bn_lt_tr : BNDB.

(* ltBN_trans *)
Lemma bn_lt_trans : forall (b1 b2 b3 : bn), b1 bn< b2 -> b2 bn< b3 -> b1 bn< b3.
Proof. intros. eapply bn_lt_tr. eexact H0. trivial. Qed.

Hint Resolve bn_lt_trans : BNDB.

(* lt_lteqBN_trans *)
Lemma bn_lt_le_trans : forall (b1 b2 b3 : bn), b1 bn< b2 -> b2 bn<= b3 -> b1 bn< b3.
Proof.
  intros.
  inversion H0.
  - rewrite H2 in H. trivial.
  - eapply bn_lt_trans. eexact H. trivial.
Qed.

Hint Resolve bn_lt_le_trans : BNDB.

(* lteqBN_trans *)
Lemma bn_le_trans : forall (b1 b2 b3 : bn), b1 bn<= b2 -> b2 bn<= b3 -> b1 bn<= b3.
Proof.
  intros.
  inversion H.
  - trivial.
  - inversion H0.
    + rewrite H5 in H. trivial.
    + constructor.
      eapply bn_lt_trans.
      eexact H1.
      trivial.
Qed.

Hint Resolve bn_le_trans : BNDB.

(* ltDs *)
Lemma bn_lt_D_Usuc : forall (b : bn), (D b) bn< (U (bn_suc b)).
Proof. intros. induction b; try(simpl; constructor; try(trivial); try(constructor)). Qed.

Hint Resolve bn_lt_D_Usuc : BNDB.

(* lts *)
Lemma bn_lt_suc : forall (b : bn), b bn< (bn_suc b).
Proof. intros. induction b; try(simpl; constructor). trivial. Qed.

Hint Resolve bn_lt_suc : BNDB.

(* lteqs *)
Lemma bn_le_suc : forall (b : bn), b bn<= (bn_suc b).
Proof.
  intros. induction b; try(simpl; constructor; constructor).
  inversion IHb.
  - contradict H0. apply bn_nsuc_id.
  - trivial.
Qed.

Hint Resolve bn_le_suc : BNDB.

(* ltpred *)
Lemma bn_lt_pre : forall (b : bn), b <> Z -> (bn_pre b) bn< b.
Proof. Admitted.

Hint Resolve bn_lt_pre : BNDB.

(* lt1 *)
Lemma bn_lt_suc_le : forall (b2 b1 : bn), b1 bn< (bn_suc b2) -> b1 bn<= b2.
Proof. Admitted.

Hint Resolve bn_lt_suc_le : BNDB.

(* lt2 *)
Lemma bn_le_lt_suc : forall (b2 b1 : bn), b1 bn<= b2 -> b1 bn< (bn_suc b2).
Proof. Admitted.

Hint Resolve bn_le_lt_suc : BNDB.

(* lteqBN_suc_pred *)
Lemma bn_le_suc_le_pre : forall (b1 b2 : bn), b1 <> Z -> b1 bn<= (bn_suc b2) -> (bn_pre b1) bn<= b2.
Proof.
  intros.
  assert ((bn_pre b1) bn< b1).
  { apply bn_lt_pre. trivial. }
  assert (bn_pre b1 bn< bn_suc b2).
  { eapply bn_lt_le_trans. eexact H1. trivial. }
  apply bn_lt_suc_le.
  trivial.
Qed.

Hint Resolve bn_le_suc_le_pre : BNDB.

(* lteqBN_refl *)
Lemma bn_le_refl : forall (b : bn), b bn<= b.
Proof. intros. constructor. Qed.

(* not_lt_suc *)
Lemma bn_lt_nsuc : forall (b1 : bn), ~ exists (b2 : bn), b1 bn< b2 /\ b2 bn< (bn_suc b1).
Proof. Admitted.

(* lt_suc_lteq *)
Lemma bn_lt_le_suc : forall (b1 b2 : bn), b1 bn< b2 -> (bn_suc b1) bn<= b2.
Proof. Admitted.

(* lteqBN_suc *)
Lemma bn_le_suc_suc : forall (b1 b2 : bn), b1 bn<= b2 -> (bn_suc b1) bn<= (bn_suc b2).
Proof.
  intros. inversion H.
  - constructor.
  - apply bn_lt_le_suc. apply bn_le_lt_suc. trivial.
Qed.

(* lteqBN_U_U *)
Lemma bn_le_UU : forall (a b : bn), (U a) bn<= (U b) -> a bn<= b.
Proof.
  intros. inversion H.
  - constructor.
  - inversion H0.
    constructor.
    trivial.
Qed.

(* lteqBN_D_D *)
Lemma bn_le_DD : forall (a b : bn), (D a) bn<= (D b)-> a bn<= b.
Proof.
  intros. inversion H.
  - constructor.
  - inversion H0.
    constructor.
    trivial.
Qed.

(* lteqBN_U_D *)
Lemma bn_le_UD : forall (a b : bn), (U a) bn<= (D b) -> a bn<= b.
Proof.
  intros. inversion H. inversion H0.
  - constructor.
  - constructor. trivial.
Qed.

(* lteqBN_D_U *)
Lemma bn_le_DU : forall (a b : bn), (D a) bn<= (U b) -> a bn<= b.
Proof.
  intros. inversion H. inversion H0.
  constructor. trivial.
Qed.

(* bbalCond_eqs *)
Lemma bbalCond_eqs: forall (s t : bn), t bn<= s -> s bn<= bn_suc t -> s = t \/ s = bn_suc t.
Proof.
  intros. inversion H.
  - intuition.
  - inversion H0.
    + intuition.
    + exfalso.
      eapply bn_lt_nsuc.
      exists s.
      split.
      * exact H1.
      * assumption.
Qed.

(* lt_U *)
Lemma bn_lt_U : forall (a b : bn), a bn< b <-> (U a) bn< U b.
Proof. Admitted.

(* lt_D *)
Lemma bn_lt_D : forall (a b : bn), a bn< b <-> (D a) bn< D b.
Proof. Admitted.
