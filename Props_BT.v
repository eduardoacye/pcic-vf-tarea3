(* Resultados de árboles de Braun vistos en clase y solicitados en la tarea *)
From Tarea Require Export Props_BN.
From Tarea Require Export Defs_BT.

Theorem eq_btree_dec: forall (s t : BTree), {s=t} + {s<>t}.
Proof. intros. decide equality. apply eq_dec_A. Qed.

Lemma nonE_tree: forall (t : BTree), t <> E -> exists (a : A) (t1 t2 : BTree), t = N a t1 t2.
Proof.
  intros. destruct t.
  - intuition.
  - exists a. exists t1. exists t2. trivial.
Qed.

Lemma bsize_Z: forall (t : BTree), bsize t = Z -> t = E.
Proof.
  intros t. destruct t.
  - intuition.
  - intros.
    simpl in H.
    symmetry in H.
    contradict H.
    apply bn_nsuc_Z.
Qed.

Lemma bsize_nonZ: forall (t : BTree), t <> E -> bsize t <> Z.
Proof.
  intros.
  contradict H.
  apply bsize_Z.
  trivial.
Qed.

Lemma btNonE: forall (t : BTree) (b : bn), t <> E -> exists (b : bn), bsize t = U b \/ bsize t = D b.
  Proof.
  intros.
  apply bsize_nonZ in H.
  apply (bn_nonZ (bsize t)) in H.
  trivial.
Qed.

Lemma prop_0_U : forall (a : A) (s t : BTree) (b : bn), 
    bbal (N a s t) -> bsize(N a s t) = U b -> 
    bsize s = b /\ bsize t = b.
Proof.
  intros.
  simpl in H0.
  assert (H0b:=H0).
  rewrite <- bn_plus_U in H0.
  apply bn_suc_eq in H0.
  inversion H.
  destruct(bbalCond_eqs (bsize s) (bsize t)).
  - trivial.
  - trivial.
  - rewrite <- H8 in H0.
    apply bn_plus_eq in H0.
    rewrite <- H8.
    intuition.
  - rewrite H8 in H0b.
    rewrite bn_plus_D in H0b.
    inversion H0b.
Qed.

Lemma prop_0_D : forall (a : A) (s t : BTree) (b : bn),
    bbal (N a s t) -> bsize(N a s t) = D b -> bsize s = bn_suc b /\ bsize t = b.
Proof.
  intros.
  simpl in H0.
  assert (H0b:=H0).
  rewrite <- bn_plus_D in H0.
  apply bn_suc_eq in H0.
  inversion H.
  destruct(bbalCond_eqs (bsize s) (bsize t)).
  - trivial.
  - trivial.
  - rewrite <- H8 in H0b.
    rewrite bn_plus_U in H0b.
    inversion H0b.
  - rewrite H8 in H0.
    apply bn_plus_suc_eq in H0.
    rewrite <- H0.
    intuition.
Qed.

Corollary size_caseU: forall (a : A) (l r : BTree) (b : bn), 
    bsize (N a l r) = U b -> bsize l = bsize r.
Proof.
  intros.
  assert (HBal := allBal (N a l r)).
  apply (prop_0_U a l r b) in H.
  - intuition.
    rewrite <- H1 in H0.
    intuition.
  - intuition.
Qed.

Corollary size_caseD: forall (a : A) (l r : BTree) (b : bn), 
    bsize (N a l r) = D b -> bsize l = bn_suc (bsize r).
Proof.
  intros.
  assert (HBal := allBal (N a l r)).
  apply (prop_0_D a l r b) in H.
  - intuition.
    rewrite <- H1 in H0.
    intuition.
  - intuition.
Qed.

Corollary bbal_size_r : forall (a : A) (l r : BTree), 
    bsize (N a l r) = U (bsize r) \/ 
    bsize (N a l r) = D (bsize r).
Proof.
  intros.
  assert (HBal:=allBal (N a l r)).
  destruct (bn_nonZ (bsize (N a l r))).
  - simpl.
    assert (Z <> bn_suc (bsize l bn+ bsize r)).
    apply bn_nsuc_Z.
    intuition.
  - destruct H.
    + apply prop_0_U in H.
      simpl.
      destruct H.
      rewrite H.
      rewrite H0.
      rewrite bn_plus_U.
      intuition.
      trivial.
    + apply prop_0_D in H.
      destruct H.
      simpl.
      rewrite H.
      rewrite H0.
      rewrite bn_plus_D.
      intuition.
      trivial.
Qed.

Theorem bbal_size_r2 : forall (a : A) (l r : BTree), (bsize (N a l r)) bn<= (D (bsize r)). 
Proof.
  intros a l r.
  destruct (bbal_size_r a l r).
  - constructor.
    rewrite H.
    constructor.
  - rewrite H.
    constructor.
Qed.

Theorem bbal_size_l: forall (a:A) (l r:BTree), (bsize (N a l r)) bn<= (U (bsize l)). 
Proof.
  intros.
  assert (HBal:=allBal (N a l r)).
  destruct (bn_nonZ (bsize (N a l r))).
  - simpl.
    assert (Z <> bn_suc (bsize l bn+ bsize r)).
    apply bn_nsuc_Z.
    intuition.
  - destruct H.
    + apply prop_0_U in H.
      * simpl.
        destruct H.
        subst.
        rewrite H0. 
        rewrite bn_plus_U.
        constructor.
      * assumption.
    +  apply prop_0_D in H.
       * simpl.
         destruct H.
         rewrite H.
         rewrite H0.
         rewrite bn_plus_D.
         constructor.
         constructor.
         apply bn_lt_suc.
       * trivial.
Qed.

Lemma lt_U_bsize : forall (b : bn) (a : A) (t1 t2 : BTree), (U b) bn< (bsize (N a t1 t2)) -> b bn< (bsize t1).
Proof.
  intros b a t1 t2 H.
  assert ((bsize (N a t1 t2)) bn<= (U (bsize t1))).
  apply bbal_size_l.
  assert ((U b) bn< (U (bsize t1))).
  eapply bn_lt_le_trans.
  eexact H.
  trivial.
  inversion H1.
  trivial.
Qed.

Theorem rightE : forall (a : A) (t1 t2 : BTree),
    bbal(N a t1 t2) -> t2 = E -> (t1 = E \/ exists (aa : A), t1 = (N aa E E)).
Proof.
  intros.
  inversion H.
  destruct (bbalCond_eqs (bsize t1) (bsize t2)).
  trivial.
  trivial.
  rewrite H0 in H8.
  simpl in H8.
  apply bsize_Z in H8.
  intuition.
  rewrite H0 in H8.
  right.
  destruct t1.
  simpl in H8.
  inversion H8.
  simpl in H8.
  replace (U Z) with (bn_suc Z) in H8.
  apply bn_suc_eq in H8.
  apply bn_plus_ZZ in H8.
  destruct H8.
  apply bsize_Z in H8.
  apply bsize_Z in H9.
  exists a1.
  rewrite H8.
  rewrite H9.
  trivial.
  intuition.
Qed.

Lemma lt_D_bsize : forall (b : bn) (a : A) (t1 t2 : BTree), (D b) bn< (bsize (N a t1 t2)) -> b bn< (bsize t2).
Proof.
  intros b a t1 t2 H.
  assert ((bsize (N a t1 t2)) bn<= (D (bsize t2))).
  apply bbal_size_r2.
  assert ((D b) bn< (D (bsize t2))).
  eapply bn_lt_le_trans.
  eexact H.
  trivial.
  inversion H1.
  trivial.
Qed.

Lemma bbal_leaf : forall (a:A), bbal (N a E E).
Proof.
  intro a.
  constructor.
  constructor.
  constructor.
  apply bn_le_refl. 
  apply bn_le_suc.
Qed.

Theorem leftE_leaf : forall (t1 t2:BTree) (a:A), bbal (N a t1 t2) -> t1 = E -> t2 = E.
Proof.
  intros t1 t2 c HBal H.
  inversion HBal.
  rewrite H in H5.
  simpl in H5.
  inversion H5.
  apply bsize_Z in H9.
  trivial.
  inversion H7.
Qed.

Lemma bbal_inv : forall (t : BTree),
    t <> E ->
    (exists (z:A), t = N z E E)  \/ 
    exists (z:A) (r1 r2:BTree),
      bbal r1 /\ bbal r2 /\ r1 <> E /\ t = N z r1 r2.
Proof.
Admitted.

Lemma lkp_upd_BN : forall (t : BTree) (x : A) (b : bn),
    t <> E -> 
    b bn< (bsize t) -> 
    lookup_bn (update t b x) b = x.
Proof.
  intros t x.
  assert (H:=allBal t).
  (*Induction on t*)
  induction t.
  - (*Base case t = E *)
    intuition.
  - (*Induction step t = N a t1 t2*)
    intros.
    (*cases on BNat number b*)
    destruct b.
    + (*1. b=Z*)
      reflexivity.
    + (*2. b = U b*)
      destruct (eq_btree_dec t1 E).
      (*Cases on t1*)
      * (*i) t1=E A*)
        assert (t2 = E).
        -- apply (leftE_leaf t1 t2 a).
           ++ eexact H.
           ++ assumption.
        -- (*t1=E  and t2=E *)
          subst.
          simpl in H1.
          inversion H1.
          inversion H4.
      * (*ii) t1<>E A*)
        simpl. 
        apply IHt1.
        -- inversion H.
           assumption.
        -- assumption.
        -- eapply lt_U_bsize.
           exact H1.
    + (*3. b = D b*)
      destruct (eq_btree_dec t2 E).
      * destruct (rightE a t1 t2).
        -- assumption.
        -- assumption.
        -- simpl.
           subst.
           simpl in H1.
           inversion H1.
           inversion H4.
        -- destruct H2.
           subst.
           simpl in H1.
           inversion H1.
           inversion H4.
      * simpl. 
        apply IHt2.
        -- inversion H.
           assumption.
        -- assumption.
        -- eapply lt_D_bsize.
           exact H1.
Qed.

Lemma lkp_upd_BNindbal : forall (t : BTree) (x : A) (b : bn),
    t <> E -> 
    b bn< (bsize t) -> 
    lookup_bn (update t b x) b = x.
Proof.
  intros t x.
  assert (H:=allBal t).
  induction H.
  - intuition.
  - intros.
    destruct b.
    + reflexivity.
    + simpl.
      destruct (eq_btree_dec s E).
      * destruct (eq_btree_dec t E).
        -- subst.
           simpl in H4.
           apply bn_lt_U in H4.
           inversion H4.
        -- subst.
           simpl in H1.
           inversion H1. 
           ++ subst.
              apply bsize_nonZ in n.
              contradiction n.  
           ++ inversion H5.
      * apply IHbbal1.
        -- assumption.
        -- apply bn_lt_U.
           eapply bn_lt_le_trans.
           ++ exact H4.
           ++ apply bbal_size_l.
    + destruct (eq_btree_dec t E).
      * destruct (eq_btree_dec s E). 
        -- subst.
           simpl in H4.
           inversion H4.
           inversion H7.
        -- subst.
           simpl in H2.
           inversion H2.
           ++ simpl in H4.
              rewrite H7 in H4.
              simpl in H4. 
              inversion H4.
              inversion H9.
           ++ subst.
              inversion H5.
              ** contradict n.
                 apply bsize_Z.
                 intuition. 
              ** inversion H8.
              ** inversion H8.
      *  simpl.
         apply IHbbal2.
         -- assumption.
         -- apply bn_lt_D.
            eapply bn_lt_le_trans.
            ++ exact H4.
            ++ apply bbal_size_r2.  
Qed.

Lemma elmnt_lkp_upd : forall (t : BTree) (i j : bn), 
    i bn< (bsize t) -> j bn< (bsize t) -> 
    i <> j -> 
    forall (x:A), 
      lookup_bn (update t i x) j = lookup_bn t j.
Proof.
  intros t.
  induction t.
  (* t = E*)
  - intros.
    simpl in H0.
    inversion H0.
  - (* t = N a t1 t2 *)
    intros.
    assert (tBal:=allBal (N a t1 t2)).
    destruct (bbal_inv (N a t1 t2)).
    + discriminate.
    + (* exists z : A, N a t1 t2 = N z E E *)
      destruct H2.
      inversion H2.
      subst.
      simpl in H.
      inversion H.
      * subst.
        simpl in H0.
        inversion H0.
        -- subst. intuition.
        -- reflexivity.
        -- reflexivity. 
      * destruct j.
        -- reflexivity.
        -- inversion H5.
        -- inversion H5.
      * inversion H5.
    + (*  exists (z : A) (r1 r2 : BTree),
         bbal r1 /\ bbal r2 /\ r1 <> E /\ N a t1 t2 = N z r1 r2 *)
      do 4 (destruct H2).
      destruct H3.
      destruct H4.
      destruct H5.
      destruct i.
      * destruct j. 
        -- intuition.
        -- reflexivity.
        -- reflexivity.
      * destruct j.
        -- reflexivity.
        -- simpl.
           apply IHt1. 
           ++ apply bn_lt_U.
              eapply bn_lt_le_trans.
              ** exact H.
              ** apply bbal_size_l. 
           ++ apply bn_lt_U.
              eapply bn_lt_le_trans.
              ** exact H0.
              ** apply bbal_size_l.
           ++ contradict H1.
              subst;reflexivity.
        -- reflexivity.
      * destruct j.
        -- reflexivity.
        -- reflexivity.
        -- simpl. 
           apply IHt2. 
           ++ apply bn_lt_D.
              eapply bn_lt_le_trans.
              ** exact H.
              ** apply bbal_size_r2.
           ++ apply bn_lt_D.
              eapply bn_lt_le_trans.
              ** exact H0.
              ** apply bbal_size_r2.
           ++ contradict H1.
              subst;reflexivity.
Qed.

Lemma bsize_upd : forall (t : BTree) (x : A) (b : bn), 
    b bn< bsize t -> bsize t = bsize (update t b x).
Proof.
  intro t.
  induction t.
  - (* Base case *)
    intuition.
    inversion H.
  - (* Inductive step *)
    intros.
    destruct (bbal_inv (N a t1 t2)).
    + discriminate.
    + destruct H0.
      rewrite H0 in H.
      simpl in H.
      inversion H.
      * (* b = Z *)
        reflexivity.
      * (* U a0 = b, a < Z *)
        inversion H3.
      * (* D a0 = b, a < Z *)
        inversion H3.
    + do 4 (destruct H0).
      destruct H1.
      destruct H2.
      inversion H3.
      subst.
      destruct b.
      * (* Z *)
        reflexivity.
      * (* U b*)
        simpl.
        rewrite (IHt1 x b).
        -- reflexivity.
        -- apply (lt_U_bsize b x0 x1 x2).
           assumption. 
      * (* b = D b *)
        simpl.
        rewrite (IHt2 x b).
        -- reflexivity.
        -- apply (lt_D_bsize b x0 x1 x2).
           assumption.
Qed.

Lemma bsize_le: forall (t:BTree) (x:A), bsize (le x t) = bn_suc (bsize t).
Proof.
  intro.
  assert (HBal := allBal t).  
  induction HBal.
  - reflexivity.
  - intro.
    simpl.
    rewrite IHHBal2.
    rewrite <- bn_plus_suc.
    rewrite bn_plus_comm.
    reflexivity.
Qed.

Lemma bal_le: forall (t:BTree), bbal t -> 
                                forall (x:A), bbal (le x t).
Proof.
  intros t HtBal.
  induction HtBal.
  - simpl.
    apply bbal_leaf.
  - intros.
    simpl.
    constructor.
    + apply IHHtBal2.
    + assumption.
    + rewrite bsize_le.
      assumption.
    + rewrite bsize_le.
      apply bn_le_suc_suc.
      assumption.
Qed.

Lemma le_head: forall (t: BTree) (x:A),  lookup_bn (le x t) Z = x.
Proof.
  intros.
  destruct t.
  - intuition.
  - intuition.
Qed.

Lemma le_idx: forall (t:BTree),
    bbal t -> 
    forall (j:bn), j bn< (bsize t) -> forall (x:A), lookup_bn (le x t) (bn_suc j) = lookup_bn t j.
Proof.
  intros t B.
  induction B.
  - intros.
    simpl in H.
    inversion H.
  - intros.
    clear IHB1.
    destruct j.
    + simpl.
      apply le_head.
    + reflexivity.
    + simpl.
      apply IHB2.
      apply (lt_D_bsize j a s t).
      assumption.
Qed.

Lemma bsize_he: forall (t:BTree) (x:A), 
    bsize (he x t) = bn_suc (bsize t).
Proof.
  intro.
  induction t.
  - intuition.
  - intros.
    destruct (bbal_size_r a t1 t2).
    + simpl in H.
      simpl.     
      rewrite H.
      simpl.
      rewrite IHt1.
      rewrite <- bn_plus_suc.
      rewrite H. 
      intuition.
    + simpl in H.
      simpl.
      rewrite H.
      simpl.
      rewrite IHt2.
      rewrite <- bn_plus_suc2.
      rewrite H.
      intuition.
Qed.

Lemma bal_he: forall (t:BTree),
    bbal t -> forall (x:A), bbal (he x t).
Proof.
  intros t Ht.
  induction t.
  - simpl.
    apply bbal_leaf.
  - intros.
    inversion Ht.
    subst.
    destruct (bbal_size_r a t1 t2).
    + assert(H6:=H).
      apply size_caseU in H.
      simpl in H6.
      simpl.
      rewrite H6.
      constructor.
      * apply IHt1.
        assumption.
      * assumption.
      * rewrite bsize_he.
        inversion H4.
        -- intuition.
        -- admit.
      * rewrite bsize_he.
        rewrite H.
        intuition.
    + assert(H6:=H).
      apply size_caseD in H.
      simpl in H6.
      simpl.
      rewrite H6.
      constructor.
Admitted.       

Lemma he_last: forall (t: BTree) (x:A),  lookup_bn (he x t) (bsize t) = x.
Admitted.

Lemma he_idx: forall (t:BTree),
    bbal t -> forall (j:bn),
      j bn< (bsize t) ->  forall (x:A),
        lookup_bn (he x t) j = lookup_bn t j.
Admitted.

(* Auxiliares para la tarea *)
Lemma bn_plus_r : forall b1 b2 b', b1 = b2 -> b1 bn+ b' = b2 bn+ b'.
Proof. intros. subst. reflexivity. Qed.

Lemma lr_def : forall a b t1 t2 t3, lr (N a (N b t1 t2) t3) = N b t3 (lr (N b t1 t2)).
Proof. simpl. reflexivity. Qed.

Lemma btree_one : forall a t t', t = (N a E t') -> t' = E.
Proof.
  intros.
  assert (H' := allBal (N a E t')).
  inversion H'. inversion H5. destruct t'.
  - reflexivity.
  - contradict H9. apply bn_neq_sym. simpl. apply bn_nsuc_Z.
  - inversion H7.
Qed.       

Lemma bsize_one : forall a t, bsize (N a E t) = U Z.
Proof.
  intros.
  assert (H := allBal (N a E t)).
  inversion H. inversion H5. subst. simpl. rewrite H9. reflexivity.
  inversion H7.
Qed.

Lemma le_node : forall x t x' t1 t2, le x t = N x' t1 t2 -> x = x'.
Proof.
  intros. induction t.
  - simpl in H. inversion H. reflexivity.
  - simpl in H. inversion H. reflexivity.
Qed.
    
(* Propiedades de la tarea *)
Theorem bsize_lr_pred : forall t, t <> E -> bsize (lr t) = bn_pre (bsize t).
Proof.
  intros.
  assert (Hbal := allBal t).
  induction t.
  - contradict H. reflexivity.
  - destruct t1.
    + simpl. inversion Hbal. inversion H5.
      rewrite H9. trivial.
      inversion H7.
    + rewrite lr_def. unfold bsize. fold bsize.
      rewrite bn_pre_suc_id.
      rewrite <- bn_plus_suc.
      apply f_equal.
      rewrite bn_plus_comm.
      apply bn_plus_r.
      rewrite IHt1.
      * simpl. rewrite bn_pre_suc_id. reflexivity.
      * intro. inversion H0.
      * apply (allBal (N a0 t1_1 t1_2)).
Qed.

Theorem bbal_lr : forall t, t <> E -> bbal t -> bbal (lr t).
Proof.
  intros.
  induction t.
  - contradict H. reflexivity.
  - destruct t1.
    + simpl. inversion H0. assumption.
    + rewrite lr_def.
      inversion H0.
      apply bbalN; try(assumption).
      * apply IHt1. intro. inversion H8.
        assumption.
      * rewrite bsize_lr_pred.
        apply bn_le_suc_le_pre.
        -- simpl. apply bn_neq_sym. apply bn_nsuc_Z.
        -- assumption.
        -- intro. inversion H8.
      * rewrite bsize_lr_pred.
        rewrite bn_suc_pre_id.
        assumption.
        inversion H0.
        simpl. apply bn_neq_sym. apply bn_nsuc_Z.
        intro. inversion H8.
Qed.

Theorem lookup_lr : forall t j,
    t <> E ->
    j bn< (bsize t) ->
    lookup_bn (lr t) j = lookup_bn t (bn_suc j).
Proof.
  intros t. induction t.
  - intros. contradict H. reflexivity.
  - destruct t1.
    + intros. rewrite bsize_one in H0. inversion H0.
      * simpl. reflexivity.
      * inversion H3.
      * inversion H3.
    + intros j. rewrite lr_def. induction j.
      * simpl. reflexivity.
      * simpl. reflexivity.
      * intros. unfold lookup_bn. fold lookup_bn.
        rewrite IHt1.
        -- simpl. reflexivity.
        -- intro. inversion H1.
        -- assert (H' := allBal (N a (N a0 t1_1 t1_2) t2)).
           inversion H'.
           apply lt_D_bsize in H0.
           subst. inversion H6.
           ++ simpl. rewrite <- H3. assumption.
           ++ eapply bn_lt_le_trans. apply H0. apply bn_le_lt. assumption.
Qed.

Theorem le_lr : forall t x, lr (le x t) = t.
Proof.
  intros t. induction t.
  - intros. simpl. reflexivity.
  - intros x.
    unfold le. fold le.
    induction (le a t2) eqn:Ele.
    + destruct t2; (simpl in Ele; discriminate Ele).
    + rewrite lr_def. rewrite <- Ele. rewrite IHt2.
      apply le_node in Ele. subst. reflexivity.
Qed.

Theorem le_lookup_lr : forall t, t <> E -> le (lookup_bn t Z) (lr t) = t.
Proof.
  intros t.
  assert (H := allBal t).
  induction t.
  - intros. contradict H0. reflexivity.
  - intros. clear H0.
    destruct t1.
    + inversion H. inversion H5.
      * apply bsize_Z in H9. subst. simpl. reflexivity.
      * inversion H7.
    + unfold lookup_bn in IHt1.
      unfold lookup_bn.
      rewrite lr_def.
      unfold le. fold le. rewrite IHt1.
      * reflexivity.
      * inversion H. assumption.
      * intro. discriminate H0.
Qed.

Lemma bsize_UZ_hr : forall a t1 t2,
    bsize (N a t1 t2) = U Z -> hr (N a t1 t2) = E.
Proof. intros. unfold hr. fold hr. rewrite H. reflexivity. Qed.

Lemma bsize_Ub_hr : forall a t1 t2 b,
    bsize (N a t1 t2) = U b -> b <> Z -> hr (N a t1 t2) = N a t1 (hr t2).
Proof.
  intros. destruct b.
  - contradict H0. reflexivity.
  - unfold hr. fold hr. rewrite H. reflexivity.
  - unfold hr. fold hr. rewrite H. reflexivity.
Qed.

Lemma bsize_D_hr : forall a t1 t2 b,
    bsize (N a t1 t2) = D b -> hr (N a t1 t2) = N a (hr t1) t2.
Proof. intros. destruct b; (unfold hr; fold hr; rewrite H; reflexivity). Qed.

Lemma bn_plus_bpre_U : forall b, b <> Z -> bn_suc (b bn+ bn_pre b) = bn_pre (U b).
Proof.
  intros. rewrite bn_plus_comm.
  rewrite bn_plus_suc. rewrite (bn_suc_pre_id b H). rewrite (bn_pre_UD b H).
  induction b.
  + contradict H. reflexivity.
  + destruct (bn_dec_eq b Z).
    * subst. simpl. reflexivity.
    * assert (bn_pre (U b) = D (bn_pre b)).
      { simpl. destruct b. contradict n. reflexivity. reflexivity. reflexivity. }
      rewrite H0. rewrite <- (IHb n).
      simpl. reflexivity.
  + simpl. apply f_equal. apply bn_plus_U.
Qed.

Theorem bsize_hr : forall t, t <> E -> bsize (hr t) = bn_pre (bsize t).
Proof.
  intros.
  assert (Hbal := allBal t).
  induction t.
  - contradict H. reflexivity.
  - destruct (bsize (N a t1 t2)) eqn:Ebsize.
    + apply bsize_Z in Ebsize. discriminate.
    + destruct (prop_0_U a t1 t2 b Hbal Ebsize) as [H1 H2].
      destruct (bn_dec_eq b Z).
      * rewrite e in *. rewrite (bsize_UZ_hr a t1 t2 Ebsize). reflexivity.
      * rewrite (bsize_Ub_hr a t1 t2 b Ebsize n).
        unfold bsize. fold bsize. rewrite H1. rewrite IHt2.
        -- rewrite H2. rewrite (bn_pre_UD b n).
           rewrite (bn_plus_bpre_U b n).
           rewrite (bn_pre_UD b n).
           reflexivity.
        -- destruct t2.
           ++ simpl in H2. symmetry in H2. contradict n. apply H2.
           ++ intro. discriminate H0.
        -- inversion Hbal. assumption.
    + destruct (prop_0_D a t1 t2 b Hbal Ebsize).
      rewrite (bsize_D_hr a t1 t2 b Ebsize).
      unfold bsize; fold bsize. rewrite H1. rewrite IHt1.
      * rewrite H0. rewrite bn_pre_suc_id. rewrite bn_plus_U.
        simpl. reflexivity.
      * destruct t1.
        -- simpl in H0. contradict H0. apply bn_nsuc_Z.
        -- intro. discriminate H2.
      * inversion Hbal. assumption.
Qed.

Theorem bbal_hr : forall t, t <> E -> bbal t -> bbal (hr t).
Proof.
  intros. induction t.
  - contradict H. reflexivity.
  - destruct (bsize (N a t1 t2)) eqn:Ebsize.
    + apply bsize_Z in Ebsize. discriminate Ebsize.
    + destruct (prop_0_U a t1 t2 b H0 Ebsize).
      destruct (bn_dec_eq b Z).
      * rewrite e in *. apply bsize_Z in H1. apply bsize_Z in H2.
        rewrite (bsize_UZ_hr a t1 t2 Ebsize). subst. inversion H0. assumption.
      * rewrite (bsize_Ub_hr a t1 t2 b Ebsize n).
        inversion H0. destruct t2.
        -- simpl in H2. symmetry in H2. contradict n. assumption.
        -- assert (N a1 t2_1 t2_2 <> E).
           { intro. discriminate H10. }
           assert (H11 := IHt2 H10 H7).
           apply bbalN.
           ++ assumption.
           ++ assumption.
           ++ rewrite bsize_hr.
              apply (bn_le_suc_le_pre (bsize (N a1 t2_1 t2_2)) (bsize t1)).
              ** intro. apply bsize_Z in H12. discriminate H12.
              ** apply (bn_le_trans (bsize (N a1 t2_1 t2_2))
                                    (bsize t1)
                                    (bn_suc (bsize t1))
                                    H8).
                 apply bn_le_suc.
              ** intro. discriminate H12.
           ++ rewrite H1.
              rewrite bsize_hr.
              rewrite H2.
              rewrite bn_suc_pre_id.
              apply bn_le_id. apply n. intro. discriminate H12.
    + destruct t1.
      * rewrite (bsize_one a t2) in Ebsize. discriminate Ebsize.
      * destruct (prop_0_D a (N a0 t1_1 t1_2) t2 b H0 Ebsize).
        rewrite (bsize_D_hr a (N a0 t1_1 t1_2) t2 b Ebsize).
        inversion H0.
        assert (N a0 t1_1 t1_2 <> E).
        { intro. discriminate. }
        apply bbalN.
        -- apply (IHt1 H10 H6).
        -- apply H7.
        -- rewrite bsize_hr; try(assumption).
           rewrite H1. rewrite bn_pre_suc_id. rewrite H2. apply bn_le_id.
        -- rewrite bsize_hr; try(assumption).
           rewrite H1. rewrite bn_pre_suc_id. rewrite H2. apply bn_le_suc.
Qed.

Lemma bn_suc_lt : forall a b, a bn< b -> bn_suc a bn< bn_suc b.
Proof.
  intros. induction H.
  - simpl. destruct b; try(constructor).
    + constructor.
    + constructor.
  - simpl. destruct b; try(simpl; constructor; constructor).
  - unfold bn_suc; fold bn_suc. apply -> bn_lt_D. assumption.
  - simpl. apply bn_lt_D_Usuc.
  - simpl.
    destruct b1, b2; try(simpl; try(constructor); try(constructor)).
    + inversion H. 
    + apply bn_lt_U. assumption.
    + simpl in IHbn_lt. inversion IHbn_lt. assumption.
    + inversion H.
    + simpl in IHbn_lt. inversion IHbn_lt.
      * apply bn_lt_suc.
      * assert (H3 := bn_lt_pre (bn_suc b1)).
        assert (bn_suc b1 <> Z).
        { apply bn_neq_sym. apply bn_nsuc_Z. }
        apply H3 in H4. rewrite bn_pre_suc_id in H4.
        apply (bn_lt_trans b1 (bn_suc b1) b2 H4 H2).
    + simpl in IHbn_lt. inversion IHbn_lt.
      apply bn_lt_D in H.
      assert (H3 := bn_lt_suc b2).
      apply (bn_lt_trans b1 b2 (bn_suc b2) H). assumption.
  - simpl.
    assert (H1 := bn_lt_le_suc b1 b2 H).
    inversion H1.
    + constructor.
    + constructor. assumption.
  - simpl. apply -> bn_lt_U. assumption.
Qed.
      
Theorem lookup_hr : forall t j, bn_suc j bn< bsize t -> lookup_bn (hr t) j = lookup_bn t j.
Proof.
  intros t.
  assert (Hbal := allBal t).
  induction t.
  - intros. simpl in H. inversion H.
  - destruct (bsize (N a t1 t2)) eqn:Ebsize.
    + apply bsize_Z in Ebsize. discriminate.
    + destruct (prop_0_U a t1 t2 b Hbal Ebsize).
      destruct (bn_dec_eq b Z).
      * intros. rewrite e in *. inversion H1.
        exfalso. apply (bn_nsuc_Z j). assumption.
        inversion H4. inversion H4.
      * intros. rewrite (bsize_Ub_hr a t1 t2 b Ebsize n).
        destruct j.
        -- simpl. reflexivity.
        -- unfold lookup_bn; fold lookup_bn. reflexivity.
        -- unfold lookup_bn; fold lookup_bn. apply IHt2.
           ++ inversion Hbal; assumption.
           ++ inversion Hbal. rewrite H0. inversion H1. assumption.
    + destruct (prop_0_D a t1 t2 b Hbal Ebsize).
      rewrite (bsize_D_hr a t1 t2 b Ebsize).
      intros. destruct j.
      * simpl. reflexivity.
      * unfold lookup_bn; fold lookup_bn. apply IHt1.
        -- inversion Hbal; assumption.
        -- rewrite H. apply bn_suc_lt.
           simpl in H1. apply bn_lt_D. assumption.
      * unfold lookup_bn; fold lookup_bn. reflexivity.
Qed.

Theorem hr_he : forall t x, hr (he x t) = t.
Proof.
  intros t.
  assert (Hbal := allBal t).
  induction t.
  - intros. simpl. reflexivity.
  - destruct (bsize (N a t1 t2)) eqn:Ebsize.
    + exfalso. simpl in Ebsize. apply (bn_nsuc_Z (bsize t1 bn+ bsize t2)).
      symmetry. assumption.
    + intros.
      unfold he; fold he. rewrite Ebsize.
      assert (bsize (N a (he x t1) t2) = D b).
      { assert (H := bsize_he t1 x).
        simpl in Ebsize. simpl. rewrite H. clear H.
        rewrite <- bn_plus_suc. rewrite Ebsize. simpl. reflexivity. }
      unfold hr; fold hr. rewrite H.
      rewrite IHt1. reflexivity. inversion Hbal; assumption.
    + intros.
      unfold he; fold he. rewrite Ebsize.
      assert (bsize (N a t1 (he x t2)) = U (bn_suc b)).
      { assert (H := bsize_he t2 x).
        simpl in Ebsize. simpl. rewrite H. clear H.
        rewrite bn_plus_comm. rewrite <- bn_plus_suc. rewrite bn_plus_comm. rewrite Ebsize.
        simpl. reflexivity. }
      unfold hr; fold hr. rewrite H.
      inversion Hbal.
      rewrite (IHt2 H4).
      assert (H7 := bn_nsuc_Z b).
      destruct (bn_suc b).
      * contradict H7. reflexivity.
      * reflexivity.
      * reflexivity.
Qed.

Theorem he_lookup_bsize_hr : forall t,
    t <> E ->
    he (lookup_bn t (bn_pre (bsize t))) (hr t) = t.
Proof.
  intros.
  assert (Hbal := allBal t).
  induction t.
  - contradict H. reflexivity.
  - destruct (bsize (N a t1 t2)) eqn:Ebsize.
    + apply bsize_Z in Ebsize. discriminate Ebsize.
    + destruct (prop_0_U a t1 t2 b Hbal Ebsize).
      destruct (bn_dec_eq b Z).
      * rewrite e in *.
        unfold bn_pre. unfold lookup_bn.
        apply bsize_Z in H0.
        apply bsize_Z in H1.
        subst. simpl. reflexivity.
      * rewrite (bn_pre_UD b n).
        unfold lookup_bn; fold lookup_bn.
        assert (t2 <> E).
        { intro. rewrite H2 in H1. simpl in H1. symmetry in H1. apply n in H1. destruct H1. }
        assert (IHt2 := IHt2 H2).
        inversion Hbal.
        assert (IHt2 := IHt2 H7).
        rewrite H1 in IHt2.
        assert (hr (N a t1 t2) = N a t1 (hr t2)).
        { unfold hr; fold hr. rewrite Ebsize. destruct b.
          contradict n. reflexivity.
          reflexivity. reflexivity. }
        rewrite H10.
        assert (bsize (N a t1 (hr t2)) = bn_pre (U b)).
        { unfold bsize; fold bsize.
          rewrite H0. rewrite (bsize_hr t2 H2).
          rewrite H1.
          rewrite (bn_plus_bpre_U b n). reflexivity. }
        unfold he; fold he.
        rewrite H11. rewrite (bn_pre_UD b n).
        rewrite IHt2. reflexivity.
    + destruct (prop_0_D a t1 t2 b Hbal Ebsize).
      rewrite (bsize_D_hr a t1 t2 b Ebsize).
      assert (t1 <> E).
      { destruct b.
        intro. rewrite H2 in H0. simpl in H0. discriminate H0.
        intro. rewrite H2 in H0. simpl in H0. discriminate H0.
        intro. rewrite H2 in H0. simpl in H0. discriminate H0. }
      unfold bn_pre.
      assert (bsize (N a (hr t1) t2) = bn_pre (D b)).
      { unfold bsize; fold bsize.
        rewrite H1. Check bsize_hr. rewrite (bsize_hr t1 H2).
        rewrite H0. rewrite bn_pre_suc_id. rewrite bn_plus_U. simpl.
        reflexivity. }
      unfold he; fold he. rewrite H3. unfold bn_pre.
      unfold lookup_bn; fold lookup_bn.
      assert (IHt1 := IHt1 H2).
      inversion Hbal.
      assert (IHt1 := IHt1 H7).
      rewrite H0 in IHt1. rewrite bn_pre_suc_id in IHt1.
      rewrite IHt1. reflexivity.
Qed.
